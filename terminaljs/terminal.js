/*! terminal.js v2.0 | (c) 2014 Erik Österberg | https://github.com/eosterberg/terminaljs ,  
2018 Alexey Galkin | https://gitlab.com/johnbrown90210/protoparser.js */

var Terminal = (function () {
	// PROMPT_TYPE
	var PROMPT_INPUT = 1, PROMPT_PASSWORD = 2, PROMPT_CONFIRM = 3
		
	var fireCursorInterval = function (inputField, terminalObj) {
		var cursor = terminalObj._cursor
		setTimeout(function () {
			if (inputField.parentElement && game.shouldBlinkCursor/*terminalObj._shouldBlinkCursor // commented by Alexey Galkin (johnbrown)*/) {
				cursor.style.visibility = cursor.style.visibility === 'visible' ? 'hidden' : 'visible'
				fireCursorInterval(inputField, terminalObj)
			} else {
				cursor.style.visibility = 'visible'
			}
		}, game.fireCursorInterval) // modified by Alexey Galkin (johnbrown)
	}

	var firstPrompt = true;
	promptInput = function (terminalObj, message, PROMPT_TYPE, callback) {
		var shouldDisplayInput = (PROMPT_TYPE === PROMPT_INPUT)
		var inputField = document.createElement('input')

		inputField.style.position = 'absolute'
		inputField.style.zIndex = '-100'
		inputField.style.outline = 'none'
		inputField.style.border = 'none'
		inputField.style.opacity = '0'
		inputField.style.fontSize = '0.2em'

		terminalObj._inputLine.textContent = game.commandTemplate // modified by Alexey Galkin (johnbrown)
		terminalObj._input.style.display = 'block'
		terminalObj.html.appendChild(inputField)
		fireCursorInterval(inputField, terminalObj)
		
		if (message.length) terminalObj.print(PROMPT_TYPE === PROMPT_CONFIRM ? message + ' (Да/Нет)' : message) // modified by Alexey Galkin (johnbrown)

		inputField.onblur = function () {
			terminalObj._cursor.style.display = 'none'
		}

		inputField.onfocus = function () {
			inputField.value = terminalObj._inputLine.textContent
			terminalObj._cursor.style.display = 'inline'
		}

		terminalObj.html.onclick = function () {
			inputField.focus()
		}

		inputField.onkeydown = function (e) {
			// arrow keys handler // added by Alexey Galkin (johnbrown)
			if (e.which === 38 || e.which === 40) {
				e.preventDefault()
				e.which === 38 ? game.commandHistoryIndex++ : game.commandHistoryIndex--
				if (game.commandHistory[game.commandHistoryIndex] === undefined) {
					if (e.which === 38) game.commandHistoryIndex = 0
					if (e.which === 40) game.commandHistoryIndex = game.commandHistory.length - 1
				}
				if (game.commandHistory[0] === undefined) {
					inputField.value = ''
					t.print(game.prompt + 'история')
					history()
					t.print('<br>')
				} 
				else inputField.value = game.commandHistory[game.commandHistoryIndex]
			}
			// end of addition
			
			if (e.which === 37 || e.which === 39 || e.which === 9) { // modified by Alexey Galkin (johnbrown)
				e.preventDefault()
			}
			// hotkeys handler // added by Alexey Galkin (johnbrown)
			if (e.which in terminalObj.keyBindings) {
				e.preventDefault()
				game.commandHistoryIndex = -1
				t.print(game.prompt + game[terminalObj.keyBindings[e.which]])
				output(game.prompt + game[terminalObj.keyBindings[e.which]])
				t.print('<br>')
				prompt.innerHTML = game.prompt // update prompt
			}
			// end of addition
			if (shouldDisplayInput && e.which !== 13) {
				setTimeout(function () {
					terminalObj._inputLine.textContent = inputField.value
				}, 1)
			}
			// scroll to bottom of terminal window to display cursor completely // added by Alexey Galkin (johnbrown)
			var termElement = document.getElementById("term")
			termElement.scrollTop = termElement.scrollHeight;
			// end of addition
		}
		// insert game.prompt before input field // added by Alexey Galkin (johnbrown)
		var prompt = document.createElement('span')
		prompt.innerHTML = game.prompt
		var inputParent = terminalObj._inputLine.parentNode
		inputParent.insertBefore(prompt, terminalObj._inputLine)
		// end of addition

		inputField.onkeyup = function (e) {
			if (PROMPT_TYPE === PROMPT_CONFIRM || e.which === 13) {
				game.commandHistoryIndex = -1 // added by Alexey Galkin (johnbrown)
				terminalObj._input.style.display = 'none'
				var inputValue = game.prompt + inputField.value // modified by Alexey Galkin (johnbrown)
				if (shouldDisplayInput) terminalObj.print(inputValue)
				terminalObj.html.removeChild(inputField)
inputParent.removeChild(prompt) // remove prompt to avoid cloning prompt element // added by Alexey Galkin (johnbrown)
				if (typeof(callback) === 'function') {
					if (PROMPT_TYPE === PROMPT_CONFIRM) {
	callback(inputValue.toUpperCase()[game.prompt.length] === 'Д' ? true : false) // modified by Alexey Galkin (johnbrown)
					} else callback(inputValue)
				}
				// scroll to bottom of terminal window to display cursor completely // added by Alexey Galkin (johnbrown)
				var termElement = document.getElementById("term")
				termElement.scrollTop = termElement.scrollHeight;
				// end of addition
			}
		}
		if (firstPrompt) {
			firstPrompt = false
			setTimeout(function () { inputField.focus()	}, 50)
		} else {
			inputField.focus()
		}
	}

	//var terminalBeep // commented by Alexey Galkin (johnbrown)

	var TerminalConstructor = function (id) {
		/* commented by Alexey Galkin (johnbrown)
		if (!terminalBeep) {
			terminalBeep = document.createElement('audio')
			var source = '<source src="http://www.erikosterberg.com/terminaljs/beep.'
			terminalBeep.innerHTML = source + 'mp3" type="audio/mpeg">' + source + 'ogg" type="audio/ogg">'
			terminalBeep.volume = 0.05
		}
		*/
		this.html = document.createElement('div')
		this.html.className = 'Terminal'
		if (typeof(id) === 'string') { this.html.id = id }

		this._innerWindow = document.createElement('div')
		this._output = document.createElement('p')
		this._inputLine = document.createElement('span') //the span element where the users input is put
		this._cursor = document.createElement('span')
		this._input = document.createElement('p') //the full element administering the user input, including cursor
		
		/* commented by Alexey Galkin (johnbrown)
		this._shouldBlinkCursor = true
		this.beep = function () {
			terminalBeep.load()
			terminalBeep.play()
		}
		*/
		this.print = function (message) {
			var newLine = document.createElement('div')
			newLine.innerHTML = message // modified by Alexey Galkin (johnbrown)
			this._output.appendChild(newLine)
			// add line to game.log // added by Alexey Galkin (johnbrown)
			game.log += message
			if (message != '<br>') game.log += '<br>'
			// end of addition
		}

		this.input = function (message, callback) {
			promptInput(this, message, PROMPT_INPUT, callback)
		}

		this.password = function (message, callback) {
			promptInput(this, message, PROMPT_PASSWORD, callback)
		}

		this.confirm = function (message, callback) {
			promptInput(this, message, PROMPT_CONFIRM, callback)
		}

		this.clear = function () {
			this._output.innerHTML = ''
		}

		this.sleep = function (milliseconds, callback) {
			setTimeout(callback, milliseconds)
		}

		this.setTextSize = function (size) {
			this._output.style.fontSize = size
			this._input.style.fontSize = size
		}

		this.setTextColor = function (col) {
			this.html.style.color = col
			this._cursor.style.background = col
		}

		this.setBackgroundColor = function (col) {
			this.html.style.background = col
		}

		this.setWidth = function (width) {
			this.html.style.width = width
		}

		this.setHeight = function (height) {
			this.html.style.height = height
		}

		this.blinkingCursor = function (bool) {
			bool = bool.toString().toUpperCase()
			this._shouldBlinkCursor = (bool === 'TRUE' || bool === '1' || bool === 'YES')
		}
		
		// added by Alexey Galkin (johnbrown)
		this.keyBindings = {
			103: 'num7Key',
			104: 'num8Key',
			105: 'num9Key',
			100: 'num4Key',
			101: 'num5Key',
			102: 'num6Key',
			97: 'num1Key',
			98: 'num2Key',
			99: 'num3Key',
			96: 'num0Key',
			110: 'decimalPointKey',
			111: 'devideKey',
			106: 'multiplyKey',
			107: 'addKey',
			112: 'f1Key',
			113: 'f2Key',
			117: 'f6Key',
			118: 'f7Key',
			119: 'f8Key'
		}
		// end of addition

		this._input.appendChild(this._inputLine)
		this._input.appendChild(this._cursor)
		this._innerWindow.appendChild(this._output)
		this._innerWindow.appendChild(this._input)
		this.html.appendChild(this._innerWindow)
		/* commented by Alexey Galkin (johnbrown)
		this.setBackgroundColor('black')
		this.setTextColor('white')
		this.setTextSize('1em')
		this.setWidth('100%')
		this.setHeight('100%')

		this.html.style.fontFamily = 'Monaco, Courier'
		this.html.style.margin = '0'
		this._innerWindow.style.padding = '10px'
		this._input.style.margin = '0'
		this._output.style.margin = '0'
		this._cursor.style.background = 'white'
		*/
		this._cursor.innerHTML = game.cursor // modified by Alexey Galkin (johnbrown)
		this._cursor.style.display = 'none' //then hide it
		this._input.style.display = 'none'
	}

	return TerminalConstructor
}())
